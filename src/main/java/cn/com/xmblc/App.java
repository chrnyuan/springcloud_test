package cn.com.xmblc;

import cn.hutool.core.date.DateUtil;

import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){

        System.out.println( "Hello World!" );
        String str = "2013-12-12 12:12:12";
        Date date = DateUtil.parseDate(str);
        System.out.println(date);
    }
}
