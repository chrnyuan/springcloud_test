package cn.com.xmblc;

import brave.sampler.Sampler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
public class FeignApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(FeignApp.class,args);
    }

    /**
     * 启动类中配置Sampler 抽样策略：Sampler.ALWAYS_SAMPLE,表示持续抽样
     * @return
     */
    @Bean
    public Sampler defaultSample(){
        return  Sampler.ALWAYS_SAMPLE;
    }
}
