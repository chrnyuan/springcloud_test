package cn.com.xmblc.controller;

import cn.com.xmblc.client.ProductClientFeign;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author:zyk
 * Date:2020/3/4
 * Description:
 */
@RestController
public class ProductController {

    @Autowired
    ProductClientFeign productClientFeign;

    @RequestMapping("products")
    public String products(){
        return productClientFeign.hi();
    }

}
