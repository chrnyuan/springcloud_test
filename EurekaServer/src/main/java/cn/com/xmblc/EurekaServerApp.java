package cn.com.xmblc;

import cn.hutool.core.date.DateUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

import java.util.Date;

/**
 * EurekaServer：服务发现和注册中心
 *
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApp
{
    public static void main( String[] args ) {
//        int port = 8761;
//    new SpringApplicationBuilder(App.class).properties("server.port"+port).run(args);
        SpringApplication.run(EurekaServerApp.class,args);
    }
}
