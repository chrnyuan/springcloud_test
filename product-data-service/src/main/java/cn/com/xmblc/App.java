package cn.com.xmblc;

import brave.sampler.Sampler;
import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 数据提供者
 *
 */
@SpringBootApplication
@EnableEurekaClient
@RestController
public class App {
    public static void main( String[] args ) {
        SpringApplication.run(App.class,args);
    }

    @RequestMapping("/hi")
    public String hi(){
        return  "this is a springcloud server producer";
    }

    /**
     * 启动类中配置Sampler 抽样策略：Sampler.ALWAYS_SAMPLE,表示持续抽样
     * @return
     */
    @Bean
    public Sampler defaultSample(){
        return  Sampler.ALWAYS_SAMPLE;
    }
}

